### 串接順序

#### 1. 複製整個 /app/EcPay 資料夾至專案內
#### 2. .env 使用正式環境變數
#### 3. 參照 EcPayController 選擇欲使用的支付方式
#### 4. 支付的內容再修改成愈填入的內容(如價格,商品名稱)


### 開立發票 (請先參閱套件說明https://github.com/as00306/laravel-einvoice)
#### 1. composer require dq/e-invoice
#### 2. 設定 .env 變數
#### 3. 複製app/Services/InvoiceService.php 至專案內
#### 4. 參照 InvoiceController 的流程
#### 5. 測試可從路由 /invoice?type={donation(捐贈) or carrier(載具)}
