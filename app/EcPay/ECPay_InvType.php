<?php

namespace App\EcPay;

/**
 * 字軌類別
 */
abstract class ECPay_InvType
{
    // 一般稅額
    const General = '07';

    // 特種稅額
    const Special = '08';
}
