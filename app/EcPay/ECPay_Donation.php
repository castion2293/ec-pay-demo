<?php

namespace App\EcPay;

/**
 * 電子發票捐贈註記
 */
abstract class ECPay_Donation
{
    // 捐贈
    const Yes = '1';

    // 不捐贈
    const No = '2';
}
