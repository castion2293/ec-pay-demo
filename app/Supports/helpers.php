<?php

if (! function_exists('array_get')) {
    /**
     * @param array $array
     * @param string $key
     * @return array|ArrayAccess|mixed
     */
    function array_get(array $array, string $key)
    {
        return  Arr::get($array, $key);
    }
}

