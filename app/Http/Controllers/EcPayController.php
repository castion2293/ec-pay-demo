<?php

namespace App\Http\Controllers;

use App\EcPay\ECPay_AllInOne;
use App\EcPay\ECPay_EncryptType;
use App\EcPay\ECPay_InvoiceState;
use App\EcPay\ECPay_InvType;
use App\EcPay\ECPay_PaymentMethod;
use App\EcPay\ECPay_TaxType;

class EcPayController extends Controller
{
    public function all()
    {
        $obj = new ECPay_AllInOne();

        //服務參數
        $obj->ServiceURL  = config('ec_pay.ec_pay_url');   //服務位置
        $obj->HashKey     = config('ec_pay.ec_pay_hash_key') ; //測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV      = config('ec_pay.ec_pay_hash_iv') ; //測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID  = config('ec_pay.ec_pay_merchant_id'); //測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = '1'; //CheckMacValue加密類型，請固定填入1，使用SHA256加密


        //基本參數(請依系統規劃自行調整)
        $MerchantTradeNo = "Test".time() ;
        $obj->Send['ReturnURL']         = config('ec_pay.ec_pay_return_url');     //付款完成通知回傳的網址
        $obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                           //訂單編號
        $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                        //交易時間
        $obj->Send['TotalAmount']       = 2300;                                       //交易金額
        $obj->Send['TradeDesc']         = "很炫喔" ;                           //交易描述
        $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::ALL ;                  //付款方式:全功能

        //訂單的商品資料
        array_push($obj->Send['Items'], array('Name' => "我的新IPHONE13", 'Price' => (int)"2300",
            'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));

        # 電子發票參數
        $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
        $obj->SendExtend['RelateNumber'] = "Nickc".time();
        $obj->SendExtend['CustomerEmail'] = 'test@ecpay.com.tw';
        $obj->SendExtend['CustomerPhone'] = '0985769875';
        $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
        $obj->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
        $obj->SendExtend['InvoiceItems'] = [];
        // 將商品加入電子發票商品列表陣列
        foreach ($obj->Send['Items'] as $info)
        {
            array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
                $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
        }
        $obj->SendExtend['InvoiceRemark'] = '測試發票備註';
        $obj->SendExtend['DelayDay'] = '0';
        $obj->SendExtend['InvType'] = ECPay_InvType::General;

        //產生訂單(auto suDelayFlagbmit至ECPay)
        $obj->CheckOut();
    }

    public function atm()
    {
        $obj = new ECPay_AllInOne();

        //服務參數
        $obj->ServiceURL  = config('ec_pay.ec_pay_url');   //服務位置
        $obj->HashKey     = config('ec_pay.ec_pay_hash_key') ;                                           //測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV      = config('ec_pay.ec_pay_hash_iv') ;                                           //測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID  = config('ec_pay.ec_pay_merchant_id');                                                     //測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = '1';                                                           //CheckMacValue加密類型，請固定填入1，使用SHA256加密


        //基本參數(請依系統規劃自行調整)
        $MerchantTradeNo = "Test".time() ;
        $obj->Send['ReturnURL']         = config('ec_pay.ec_pay_return_url');    //付款完成通知回傳的網址
        $obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                          //訂單編號
        $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                       //交易時間
        $obj->Send['TotalAmount']       = 2000;                                      //交易金額
        $obj->Send['TradeDesc']         = "很炫喔" ;                          //交易描述
        $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::ATM ;                 //付款方式:ATM

        //訂單的商品資料
        array_push($obj->Send['Items'], array('Name' => "我的新IPHONE13", 'Price' => (int)"23000",
            'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));

        //ATM 延伸參數(可依系統需求選擇是否代入)
        $obj->SendExtend['ExpireDate'] = 3 ;     //繳費期限 (預設3天，最長60天，最短1天)
        $obj->SendExtend['PaymentInfoURL'] = ""; //伺服器端回傳付款相關資訊。

        # 電子發票參數
       $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
       $obj->SendExtend['RelateNumber'] = "Test".time();
       $obj->SendExtend['CustomerEmail'] = 'test@ecpay.com.tw';
       $obj->SendExtend['CustomerPhone'] = '0911222333';
       $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
       $obj->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
       $obj->SendExtend['InvoiceItems'] = array();
       // 將商品加入電子發票商品列表陣列
       foreach ($obj->Send['Items'] as $info)
       {
           array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
               $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
       }
       $obj->SendExtend['InvoiceRemark'] = '測試發票備註';
       $obj->SendExtend['DelayDay'] = '0';
       $obj->SendExtend['InvType'] = ECPay_InvType::General;

        //產生訂單(auto submit至ECPay)
        $obj->CheckOut();
    }

    public function barcode()
    {
        $obj = new ECPay_AllInOne();

        //服務參數
        $obj->ServiceURL  = config('ec_pay.ec_pay_url');   //服務位置
        $obj->HashKey     = config('ec_pay.ec_pay_hash_key') ;                                           //測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV      = config('ec_pay.ec_pay_hash_iv') ;                                           //測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID  = config('ec_pay.ec_pay_merchant_id');                                                     //測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = '1';                                                           //CheckMacValue加密類型，請固定填入1，使用SHA256加密


        //基本參數(請依系統規劃自行調整)
        $MerchantTradeNo = "Test".time() ;
        $obj->Send['ReturnURL']         = config('ec_pay.ec_pay_return_url');    //付款完成通知回傳的網址
        $obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                          //訂單編號
        $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                       //交易時間
        $obj->Send['TotalAmount']       = 2000;                                      //交易金額
        $obj->Send['TradeDesc']         = "很炫喔" ;                          //交易描述
        $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::BARCODE ;             //付款方式:BARCODE超商代碼

        //訂單的商品資料
        array_push($obj->Send['Items'], array('Name' => "我的新IPHONE13", 'Price' => (int)"2000",
            'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));


        //BARCODE超商條碼延伸參數(可依系統需求選擇是否代入)
        $obj->SendExtend['Desc_1']            = '';      //交易描述1 會顯示在超商繳費平台的螢幕上。預設空值
        $obj->SendExtend['Desc_2']            = '';      //交易描述2 會顯示在超商繳費平台的螢幕上。預設空值
        $obj->SendExtend['Desc_3']            = '';      //交易描述3 會顯示在超商繳費平台的螢幕上。預設空值
        $obj->SendExtend['Desc_4']            = '';      //交易描述4 會顯示在超商繳費平台的螢幕上。預設空值
        $obj->SendExtend['PaymentInfoURL']    = '';      //預設空值
        $obj->SendExtend['ClientRedirectURL'] = '';      //預設空值
        $obj->SendExtend['StoreExpireDate']   = '';      //預設空值

        # 電子發票參數
        $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
        $obj->SendExtend['RelateNumber'] = "Test".time();
        $obj->SendExtend['CustomerEmail'] = 'test@ecpay.com.tw';
        $obj->SendExtend['CustomerPhone'] = '0911222333';
        $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
        $obj->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
        $obj->SendExtend['InvoiceItems'] = array();
        // 將商品加入電子發票商品列表陣列
        foreach ($obj->Send['Items'] as $info)
        {
            array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
                $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
        }
        $obj->SendExtend['InvoiceRemark'] = '測試發票備註';
        $obj->SendExtend['DelayDay'] = '0';
        $obj->SendExtend['InvType'] = ECPay_InvType::General;

        //產生訂單(auto submit至ECPay)
        $obj->CheckOut();
    }

    public function cvs()
    {
        $obj = new ECPay_AllInOne();

        //服務參數
        $obj->ServiceURL  = config('ec_pay.ec_pay_url');    //服務位置
        $obj->HashKey     = config('ec_pay.ec_pay_hash_key');                                            //測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV      = config('ec_pay.ec_pay_hash_iv');                                            //測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID  = config('ec_pay.ec_pay_merchant_id');                                                      //測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = '1';                                                            //CheckMacValue加密類型，請固定填入1，使用SHA256加密


        //基本參數(請依系統規劃自行調整)
        $MerchantTradeNo = "Test".time() ;
        $obj->Send['ReturnURL']         = config('ec_pay.ec_pay_return_url');     //付款完成通知回傳的網址
        $obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                           //訂單編號
        $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                        //交易時間
        $obj->Send['TotalAmount']       = 2000;                                       //交易金額
        $obj->Send['TradeDesc']         = "很炫喔" ;                           //交易描述
        $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::CVS ;                  //付款方式:CVS超商代碼

        //訂單的商品資料
        array_push($obj->Send['Items'], array('Name' => "我的新IPHONE13", 'Price' => (int)"2000",
            'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));


        //CVS超商代碼延伸參數(可依系統需求選擇是否代入)
        $obj->SendExtend['Desc_1']            = '';      //交易描述1 會顯示在超商繳費平台的螢幕上。預設空值
        $obj->SendExtend['Desc_2']            = '';      //交易描述2 會顯示在超商繳費平台的螢幕上。預設空值
        $obj->SendExtend['Desc_3']            = '';      //交易描述3 會顯示在超商繳費平台的螢幕上。預設空值
        $obj->SendExtend['Desc_4']            = '';      //交易描述4 會顯示在超商繳費平台的螢幕上。預設空值
        $obj->SendExtend['PaymentInfoURL']    = '';      //預設空值
        $obj->SendExtend['ClientRedirectURL'] = '';      //預設空值
        $obj->SendExtend['StoreExpireDate']   = '';      //預設空值

        # 電子發票參數
        $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
        $obj->SendExtend['RelateNumber'] = "Test".time();
        $obj->SendExtend['CustomerEmail'] = 'test@ecpay.com.tw';
        $obj->SendExtend['CustomerPhone'] = '0911222333';
        $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
        $obj->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
        $obj->SendExtend['InvoiceItems'] = array();
        // 將商品加入電子發票商品列表陣列
        foreach ($obj->Send['Items'] as $info)
        {
            array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
                $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
        }
        $obj->SendExtend['InvoiceRemark'] = '測試發票備註';
        $obj->SendExtend['DelayDay'] = '0';
        $obj->SendExtend['InvType'] = ECPay_InvType::General;

        //產生訂單(auto submit至ECPay)
        $obj->CheckOut();
    }

    public function credit()
    {
        try {
            $obj = new ECPay_AllInOne();

            //服務參數
            $obj->ServiceURL  = config('ec_pay.ec_pay_url'); //服務位置
            $obj->HashKey     = config('ec_pay.ec_pay_hash_key');                                          //測試用Hashkey，請自行帶入ECPay提供的HashKey
            $obj->HashIV      = config('ec_pay.ec_pay_hash_iv');                                          //測試用HashIV，請自行帶入ECPay提供的HashIV
            $obj->MerchantID  = config('ec_pay.ec_pay_merchant_id');;                                                     //測試用MerchantID，請自行帶入ECPay提供的MerchantID
            $obj->EncryptType = '1';                                                           //CheckMacValue加密類型，請固定填入1，使用SHA256加密


            //基本參數(請依系統規劃自行調整)
            $MerchantTradeNo = "Test".time() ;
            $obj->Send['ReturnURL']         = config('ec_pay.ec_pay_return_url');    //付款完成通知回傳的網址
            $obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                          //訂單編號
            $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                       //交易時間
            $obj->Send['TotalAmount']       = 2400;                                      //交易金額
            $obj->Send['TradeDesc']         = "很炫喔" ;                          //交易描述
            $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::Credit ;              //付款方式:Credit
            $obj->Send['IgnorePayment']     = ECPay_PaymentMethod::GooglePay ;           //不使用付款方式:GooglePay

            //訂單的商品資料
            array_push($obj->Send['Items'], array('Name' => "我的新IPHONE13", 'Price' => (int)"2400",
                'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));


            //Credit信用卡分期付款延伸參數(可依系統需求選擇是否代入)
            //以下參數不可以跟信用卡定期定額參數一起設定
            $obj->SendExtend['CreditInstallment'] = '' ;    //分期期數，預設0(不分期)，信用卡分期可用參數為:3,6,12,18,24
            $obj->SendExtend['InstallmentAmount'] = 0 ;    //使用刷卡分期的付款金額，預設0(不分期)
            $obj->SendExtend['Redeem'] = false ;           //是否使用紅利折抵，預設false
            $obj->SendExtend['UnionPay'] = false;          //是否為聯營卡，預設false;

            # 電子發票參數
            $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
            $obj->SendExtend['RelateNumber'] = "Nickj".time();
            $obj->SendExtend['CustomerEmail'] = 'test@ecpay.com.tw';
            $obj->SendExtend['CustomerPhone'] = '0911222333';
            $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
            $obj->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
            $obj->SendExtend['InvoiceItems'] = array();
            // 將商品加入電子發票商品列表陣列
            foreach ($obj->Send['Items'] as $info)
            {
                array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
                    $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
            }
            $obj->SendExtend['InvoiceRemark'] = '測試發票備註';
            $obj->SendExtend['DelayDay'] = '0';
            $obj->SendExtend['InvType'] = ECPay_InvType::General;

            //產生訂單(auto submit至ECPay)
            $obj->CheckOut();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function webAtm()
    {
        $obj = new ECPay_AllInOne();

        //服務參數
        $obj->ServiceURL  = config('ec_pay.ec_pay_url'); //服務位置
        $obj->HashKey     = config('ec_pay.ec_pay_hash_key');                                         //測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV      = config('ec_pay.ec_pay_hash_iv');                                         //測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID  = config('ec_pay.ec_pay_merchant_id');                                                   //測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = '1';                                                         //CheckMacValue加密類型，請固定填入1，使用SHA256加密


        //基本參數(請依系統規劃自行調整)
        $MerchantTradeNo = "Test".time() ;
        $obj->Send['ReturnURL']         = config('ec_pay.ec_pay_return_url');     //付款完成通知回傳的網址
        $obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                           //訂單編號
        $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                        //交易時間
        $obj->Send['TotalAmount']       = 2000;                                       //交易金額
        $obj->Send['TradeDesc']         = "很炫喔" ;                           //交易描述
        $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::WebATM ;               //付款方式:WebATM

        //訂單的商品資料
        array_push($obj->Send['Items'], array('Name' => "我的新IPHONE13", 'Price' => (int)"2000",
            'Currency' => "元", 'Quantity' => (int) "1", 'URL' => "dedwed"));

        # 電子發票參數
       $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
       $obj->SendExtend['RelateNumber'] = "Test".time();
       $obj->SendExtend['CustomerEmail'] = 'test@ecpay.com.tw';
       $obj->SendExtend['CustomerPhone'] = '0911222333';
       $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
       $obj->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
       $obj->SendExtend['InvoiceItems'] = array();
       // 將商品加入電子發票商品列表陣列
       foreach ($obj->Send['Items'] as $info)
       {
           array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
               $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
       }
       $obj->SendExtend['InvoiceRemark'] = '測試發票備註';
       $obj->SendExtend['DelayDay'] = '0';
       $obj->SendExtend['InvType'] = ECPay_InvType::General;

        //產生訂單(auto submit至ECPay)
        $obj->CheckOut();
    }

    public function serverReplyPayment()
    {
        // 收到綠界科技的付款結果訊息，並判斷檢查碼是否相符
        $AL = new ECPay_AllInOne();
        $AL->MerchantID = config('ec_pay.ec_pay_merchant_id');
        $AL->HashKey = config('ec_pay.ec_pay_hash_key');
        $AL->HashIV = config('ec_pay.ec_pay_hash_iv');
        // $AL->EncryptType = ECPay_EncryptType::ENC_MD5;  // MD5
        $AL->EncryptType = ECPay_EncryptType::ENC_SHA256; // SHA256
        $feedback = $AL->CheckOutFeedback();

        // 以付款結果訊息進行相對應的處理
        /**
        回傳的綠界科技的付款結果訊息如下:
        Array
        (
        [MerchantID] =>
        [MerchantTradeNo] =>
        [StoreID] =>
        [RtnCode] =>
        [RtnMsg] =>
        [TradeNo] =>
        [TradeAmt] =>
        [PaymentDate] =>
        [PaymentType] =>
        [PaymentTypeChargeFee] =>
        [TradeDate] =>
        [SimulatePaid] =>
        [CustomField1] =>
        [CustomField2] =>
        [CustomField3] =>
        [CustomField4] =>
        [CheckMacValue] =>
        )
         */

        // 在網頁端回應 1|OK
        echo '1|OK';
    }
}
