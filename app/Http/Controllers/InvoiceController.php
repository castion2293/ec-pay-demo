<?php

namespace App\Http\Controllers;

use App\Services\InvoiceService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

//use DQ\EInvoice\EInvoiceService;

class InvoiceController extends Controller
{
    public function createInvoice(Request $request)
    {
        $config = config('services.einvoice');

        $pkg = new InvoiceService($config);

        $RelateNumber = 'Nickq'. date('YmdHis') . rand(1000000000, 2147483647) ; // 產生測試用自訂訂單編號

        // 發票品項
        $items = [
            ['ItemName' => '商品名稱一', 'ItemCount' => 1, 'ItemWord' => '批', 'ItemPrice' => 100, 'ItemTaxType' => 1, 'ItemAmount' => 100, 'ItemRemark' => '商品備註一'],
            ['ItemName' => '商品名稱二', 'ItemCount' => 1, 'ItemWord' => '批', 'ItemPrice' => 150, 'ItemTaxType' => 1, 'ItemAmount' => 150, 'ItemRemark' => '商品備註二'],
            ['ItemName' => '商品名稱二', 'ItemCount' => 1, 'ItemWord' => '批', 'ItemPrice' => 250, 'ItemTaxType' => 1, 'ItemAmount' => 250, 'ItemRemark' => '商品備註三'],
        ];

        // 發票資訊
        $infos = [
            'RelateNumber' => $RelateNumber,
            'CustomerID' => '',
            'CustomerIdentifier' => '',
            'CustomerName' => '',
            'CustomerAddr' => '',
            'CustomerPhone' => '',
            'CustomerEmail' => 'castion2293@yahoo.com.tw',
            'ClearanceMark' => '',
            'Print' => 0,
            'Donation' => 2,
            'LoveCode' => '',
            'CarruerType' => '',
            'CarruerNum' => '',
            'TaxType' => 1,
            'SalesAmount' => 500,
            'InvoiceRemark' => '備註',
            'InvType' => '07',
            'vat' => '',
            'Items' => $items,
        ];

        $type = $request->input('type', '');
        // 依照不同發票類型 選擇不同的參數
        $infos = $this->selectType($infos, $type);

        // 發票開立
        $response = $pkg->einvoice($infos);
        dump('開立發票: ', $response);

        // 發送 Email 通知
        $invoice = new InvoiceService($config);

        $invoiceInfos = [
            'InvoiceNo' => Arr::get($response, 'InvoiceNumber'),
            'NotifyMail' => 'castion2293@yahoo.com.tw',
            'Notify' => 'E',
            'InvoiceTag' => 'I',
            'Notified' => 'C'
        ];

        $invoiceResponse = $invoice->invoiceNofity($invoiceInfos);
        dump('發 Email 通知:', $invoiceResponse);
    }

//    public function InvoiceNotify()
//    {
//        $config = config('services.einvoice');
//
//        $pkg = new InvoiceService($config);
//
//        $infos = [
//            'InvoiceNo' => 'NQ31014143',
//            'NotifyMail' => 'castion2293@yahoo.com.tw',
//            'Notify' => 'E',
//            'InvoiceTag' => 'I',
//            'Notified' => 'C'
//        ];
//
//        $response = $pkg->invoiceNofity($infos);
//        dump($response);
//    }

    /**
     * 依照不同發票類型 選擇不同的參數
     *
     * @param array $infos
     * @param string $type
     * @return array
     */
    private function selectType(array $infos, string $type): array
    {
        // 捐贈
        if ($type === 'donation') {
            $infos['Donation'] = 1;
            $infos['LoveCode'] = '168001';

            return $infos;
        }

        // 手機載具
        if ($type === 'carrier') {
            $infos['CarruerType'] = 3;
            $infos['CarruerNum'] = '/1234567';

            return $infos;
        }

        return $infos;
    }
}
