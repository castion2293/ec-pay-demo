<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/all', 'EcPayController@all');
Route::get('/atm', 'EcPayController@atm');
Route::get('/barcode', 'EcPayController@barcode');
Route::get('/cvs', 'EcPayController@cvs');
Route::get('/web-atm', 'EcPayController@webAtm');
Route::get('/credit', 'EcPayController@credit');

Route::get('/replay-payment', 'EcPayController@serverReplyPayment');

Route::get('/invoice', 'InvoiceController@createInvoice');
//Route::get('/invoice-notify', 'InvoiceController@InvoiceNotify');
