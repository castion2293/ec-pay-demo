<?php

return [
    // 服務位置
    'ec_pay_url' => env('EC_PAY_URL', 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5'),
    // HASH KEY
    'ec_pay_hash_key' => env('EC_PAY_HASH_KEY', '5294y06JbISpM5x9'),
    // HASH IV
    'ec_pay_hash_iv' => env('EC_PAY_HASH_IV', 'v77hoKGq4kWxNNIS'),
    // 特店編號
    'ec_pay_merchant_id' => env('EC_PAY_MERCHANT_ID', '2000132'),

    // 付款完成通知回傳的網址
    'ec_pay_return_url' => env('EC_PAY_RETURN_URL', 'http://www.ecpay.com.tw/receive.php')
];
